package com.example.appgateway.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoleConfig {
    @Value("${serviceUsername}")
    private String serviceUsernameKey;
    @Value("${servicePassword}")
    private String servicePasswordKey;

    @Value("${service.gatewayServiceUsername}")
    private String gatewayServiceUsername;
    @Value("${service.gatewayServicePassword}")
    private String gatewayServicePassword;

    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {
        return builder
                .routes()
                .route(
                        r -> r.path("/api/open-auth/**")
                                .filters(f -> f.addRequestHeader(serviceUsernameKey,gatewayServiceUsername)
                                        .addRequestHeader(servicePasswordKey,gatewayServicePassword))
                                .uri("lb://AUTH-SERVICE")
                )
                .route(
                        r -> r.path("/api/open-product/**")
                                .filters(f -> f.addRequestHeader(serviceUsernameKey,gatewayServiceUsername)
                                        .addRequestHeader(servicePasswordKey,gatewayServicePassword))
                                .uri("lb://PRODUCT-SERVICE")
                )
                .route(
                        r -> r.path("/api/open-order/**")
                                .filters(f -> f.addRequestHeader(serviceUsernameKey,gatewayServiceUsername)
                                        .addRequestHeader(servicePasswordKey,gatewayServicePassword))
                                .uri("lb://ORDER-SERVICE")
                )
                .build();
    }
}